//
//  BacketViewController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 28/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class BasketViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
  

    @IBOutlet weak var tableView: UITableView!
    static var inBasket: [Item] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*
        tableView = UITableViewController()
        tableView.tableView.dataSource = self
        tableView.tableView.delegate = self
        tableView.tableView.register(BasketCell.self, forCellReuseIdentifier: "basketCell")
 
        
        view.addSubview(tableView.view)
        addChild(tableView)
        */
 }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BasketViewController.inBasket.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "basketCell", for: indexPath) as! BasketCell
        
        weak var _data = BasketViewController.inBasket[indexPath.row]
        
        let nameFont = UIFont.preferredFont(forTextStyle: .headline)
        let nameTextColor = UIColor(displayP3Red: 0.175, green: 0.458, blue: 0.831, alpha: 1)
        let costFont = UIFont.preferredFont(forTextStyle: .subheadline)
        let costTextColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.6, alpha: 1)
        
        var attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: nameTextColor,
            .font: nameFont,
            .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle
        ]
        
        let attributedName = NSAttributedString(string: _data!.name, attributes: attributes)
        
        attributes = [
            .foregroundColor: costTextColor,
            .font: costFont
        ]
        
        let attributedCost = NSAttributedString(string:  (String(_data!.cost)), attributes: attributes)
        
        cell.nameView.attributedText = attributedName
        cell.costView.attributedText = attributedCost
        cell.imgView.image = _data!.img[0]
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        cell.cellView.layer.cornerRadius = 10
        cell.cellView.layer.masksToBounds = true
        
        return cell
    }
    
    @IBAction func buttonPressed(sender: UIButton!)
    {
        BasketViewController.inBasket.remove(at: sender.tag)
        tableView.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
}
