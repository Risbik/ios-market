//
//  popUpInfoController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class PopUpViewController: UIViewController, UIScrollViewDelegate {
    
    //@IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var costView: UILabel!
    @IBOutlet weak var descView: UILabel!
    
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var scrollView: UIScrollView!
    
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    
    
    weak var data: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        pageControl.numberOfPages = data!.img.count
        
        for index in 0 ..< data!.img.count {
            frame.origin.x = scrollView.frame.size.width * CGFloat(index)
            frame.size = scrollView.frame.size
            
            let imgView = UIImageView(frame: frame)
            weak var img = data!.img[index]
            imgView.image = img
            imgView.contentMode = .scaleAspectFit
            self.scrollView.addSubview(imgView)
        }
        
        scrollView.contentSize = CGSize(width: (scrollView.frame.width * CGFloat(data!.img.count)), height: scrollView.frame.height)
        scrollView.delegate = self
        scrollView.isPagingEnabled = true
        
        Update()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber: Int = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = pageNumber
    }
    
    func Update() {
        
        
        var nameFont = UIFont.preferredFont(forTextStyle: .headline)
        nameFont = nameFont.withSize(31)
        let nameTextColor = UIColor(displayP3Red: 0.175, green: 0.458, blue: 0.831, alpha: 1)
        
        var costFont = UIFont.preferredFont(forTextStyle: .subheadline)
        costFont = costFont.withSize(21)
        let costTextColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.6, alpha: 1)
        
        var descFont = UIFont.preferredFont(forTextStyle: .subheadline)
        descFont = descFont.withSize(24)
        let descColor = UIColor(displayP3Red: 0.8, green: 0.5, blue: 0.5, alpha: 1)
        
        var attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: nameTextColor,
            .font: nameFont,
            .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle
        ]
        
        let attributedName = NSAttributedString(string: data!.name, attributes: attributes)
        
        attributes = [
            .foregroundColor: costTextColor,
            .font: costFont
        ]
        
        let attributedCost = NSAttributedString(string:  (String(data!.cost)), attributes: attributes)
        
        attributes =
            [
                .foregroundColor: descColor,
                    .font: descFont,
                    .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle
        ]
        
        let attributedDesc = NSAttributedString(string: data!.desc, attributes: attributes)
        
        nameView.attributedText = attributedName
        costView.attributedText = attributedCost
        descView.attributedText = attributedDesc
        //imgView.image = data!.img[0]
    }
    
    @IBAction func addToBasket(_ sender: Any) {
        BasketViewController.self.inBasket.append(data!)
    }
    
    /*
    @IBAction func showImg(_ sender: Any) {
        let imageViewController = ImageViewController()
        imageViewController.data = data
        
        self.navigationController?.pushViewController(imageViewController, animated: true)
    }
 */
}
