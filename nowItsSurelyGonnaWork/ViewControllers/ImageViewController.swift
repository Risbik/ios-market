//
//  ImageViewController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 02/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    var imageView: ImageView!
    weak var data: Item?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView = ImageView(_images: data!.img)
        view.addSubview(imageView)
        
        view.backgroundColor = .black
        
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 240).isActive = true
        imageView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -120).isActive = true
        imageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
    }
    

    
 
    



}
