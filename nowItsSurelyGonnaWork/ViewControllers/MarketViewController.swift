//
//  MainController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 27/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class MarketViewController: UIViewController, IVCDelegate {

    
    var shown = false
    var marketController: UIViewController!
    var menuController: UIViewController!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        addMenuViewController()
        addItemViewController()
        
        
        //marketController.view.translatesAutoresizingMaskIntoConstraints = false
        //marketController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 100).isActive = true
    }
    
    func addItemViewController()
    {
        let itemViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Main") as! ItemViewController
        itemViewController._delegate = self
        marketController = itemViewController
        view.addSubview(marketController.view)
        addChild(marketController)
        
    }
    
    func addMenuViewController()
    {
        if (menuController == nil)
        {
            menuController = MenuViewController()
            
            view.insertSubview(menuController.view, at: 0)
            addChild(menuController)
        }
    }
    
    func toggleMenu()
    {
        addMenuViewController()
        showHideMenu()
    }
    
    func showHideMenu()
    {
        if (!shown)
        {
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.marketController.view.frame.origin.x = self.marketController.view.frame.width - 140
            })
        }
        else
        {
            UIView.animate(withDuration: 0.5,
                           animations: {
                            self.marketController.view.frame.origin.x = 0
            })
            
        }
        shown = !shown
    }
    @IBAction func showMenu(_ sender: Any) {
        toggleMenu()
    }
    
    

}
