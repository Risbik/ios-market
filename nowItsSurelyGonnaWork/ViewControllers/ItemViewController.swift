//
//  ViewController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

protocol IVCDelegate {
    func toggleMenu()
}

class ItemViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    //@IBOutlet weak var tableView: UITableView!
    @IBOutlet var sortButtons: [UIButton]!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var _delegate: IVCDelegate?
    var data: [Item] = []
    var sortedByPrice = false
    var sortedByName = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var _imgs: [UIImage?] = []
        
        //cola set up
        _imgs = [UIImage(named: "cola"),
                 UIImage(named: "cola2"),
                 UIImage(named: "cola3"),
                 UIImage(named: "cola4")]
        var temp: Item = Item("cola",
                              30,
                              "It's a cola. Kinda good drink from USA (but I'm not sure). You should try it out",
                              _imgs)
        data.append(temp)
        //pepsi set up
        _imgs = [UIImage(named: "pepsi"),
                 UIImage(named: "pepsi2"),
                 UIImage(named: "pepsi3"),
                 UIImage(named: "pepsi4")]
        temp = Item("pepsi",
                    25,
                    "Not a cola, but pepsi. Steel good drink, actualy, like cola, but have different name",
                    _imgs)
        data.append(temp)
        //7up set up
        _imgs = [UIImage(named: "7up"),
                 UIImage(named: "7up2"),
                 UIImage(named: "7up3"),
                 UIImage(named: "7up4")]
        temp = Item("7up",
                    27,
                    "Is anybody know what is that? No? Me too. What does '7' mean, what is 'up', dunno anything abot this",
                    _imgs)
        data.append(temp)
        
        
        //view.backgroundColor = .lightGray

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        
        let popUpInfo = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "info") as! PopUpViewController
        let index = indexPath.row
        
        popUpInfo.data = data[index]

    self.navigationController?.pushViewController(popUpInfo, animated: true)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell  {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "_cell", for: indexPath) as! DataCell
        
        weak var _data = data[indexPath.row]
        
        let nameFont = UIFont.preferredFont(forTextStyle: .headline)
        let nameTextColor = UIColor(displayP3Red: 0.175, green: 0.458, blue: 0.831, alpha: 1)
        let costFont = UIFont.preferredFont(forTextStyle: .subheadline)
        let costTextColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.6, alpha: 1)
        
        var attributes: [NSAttributedString.Key: Any] = [
            .foregroundColor: nameTextColor,
            .font: nameFont,
            .textEffect: NSAttributedString.TextEffectStyle.letterpressStyle
        ]
        
        let attributedName = NSAttributedString(string: _data!.name, attributes: attributes)
        
        attributes = [
            .foregroundColor: costTextColor,
            .font: costFont
        ]
        
        let attributedCost = NSAttributedString(string:  (String(_data!.cost)), attributes: attributes)
        
        cell.nameView.attributedText = attributedName
        cell.costView.attributedText = attributedCost
        cell.imgView.image = _data!.img[0]
        cell.descView.text = _data!.desc
        cell.moreView.text = "More"
        cell.cellView.layer.cornerRadius = 15
        cell.cellView.layer.masksToBounds = true
        return cell
    }
    
    @IBAction func sortButton(_ sender: Any)
    {
        showHideButtons()
    }
    
    func showHideButtons()
    {
        sortButtons.forEach {(button) in
            UIView.animate(withDuration: 0.3, animations: {
                button.isHidden = !button.isHidden
                self.view.layoutIfNeeded()
            })
        }    }
    
    @IBAction func sortByPrice(_ sender: Any) {
        if (sortedByPrice)
        {
            data.sort(by: {$0.cost > $1.cost})
            
        }
        else
        {
            data.sort(by: {$0.cost < $1.cost})
        }
        sortedByPrice = !sortedByPrice
        showHideButtons()
        collectionView.reloadData()        
    }
    
    
    @IBAction func sortByName(_ sender: Any) {
        if (sortedByName)
        {
            data.sort(by: {$0.name > $1.name})
        }
        else
        {
            data.sort(by: {$0.name < $1.name})
        }
        sortedByName = !sortedByName
        showHideButtons()
        collectionView.reloadData()
        
    }
    
    

    @IBAction func showHideMene(_ sender: Any) {
        _delegate?.toggleMenu()
    }
    
}

