//
//  MenuViewController.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 27/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    

    var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(MenuCell.self, forCellReuseIdentifier: MenuCell.self.cellID)
        view.addSubview(tableView)
        tableView.frame = view.bounds
        
        tableView.separatorStyle = .none
        tableView.rowHeight = 100
        
        tableView.backgroundColor = .darkGray
    }
    

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MenuCell.self.cellID) as! MenuCell
        let menuModel = MenuModel(rawValue: indexPath.row)
        
        cell.imgView.image = menuModel?.image
        cell.label.text = menuModel?.description
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let menuModel = MenuModel(rawValue: indexPath.row)
        
        if (menuModel?.description == "Basket")
        {
            let basketController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "basket") as! BasketViewController
            
            navigationController?.pushViewController(basketController, animated: true)
        }

        
        
    }

}
