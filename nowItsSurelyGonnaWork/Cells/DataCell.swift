//
//  customCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class DataCell: UICollectionViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var costView: UILabel!
    @IBOutlet weak var descView: UILabel!
    @IBOutlet weak var moreView: UILabel!
    
    //@IBOutlet weak var cellView: UIView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }



}
