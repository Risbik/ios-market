//
//  MenuCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 27/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    static let cellID = "menuCell"
    
    let imgView: UIImageView =
    {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.clipsToBounds = true
        
        return iv
    }()
    
    let label: UILabel =
    {
       let lb = UILabel()
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.font = UIFont.systemFont(ofSize: 20)
        return lb
    }()
    
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        addSubview(imgView)
        addSubview(label)
        

        imgView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        imgView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        imgView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        
        
        label.centerYAnchor.constraint(equalTo: imgView.centerYAnchor).isActive = true
        label.leadingAnchor.constraint(equalTo: imgView.leadingAnchor, constant: 60).isActive = true
        label.textColor = .white
        
        
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        super.setSelected(false, animated: animated)
        
        
        
    }

}
