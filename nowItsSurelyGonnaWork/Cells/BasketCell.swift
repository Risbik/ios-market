//
//  BasketCell.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 01/03/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class BasketCell: UITableViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameView: UILabel!
    @IBOutlet weak var costView: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    
/*
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        
        
     
        self.heightAnchor.constraint(equalToConstant: 100)
        
        imgView = UIImageView()
        deleteButton = UIButton()
        nameView = UILabel()
        costView = UILabel()
        
        addSubview(imgView)
        addSubview(nameView)
        addSubview(costView)
        addSubview(deleteButton)
        

        imgView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive
            = true
        imgView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imgView.heightAnchor.constraint(equalToConstant: 60).isActive = true
        imgView.widthAnchor.constraint(equalTo: imgView.heightAnchor).isActive = true
        imgView.contentMode = .scaleAspectFill

        deleteButton.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        deleteButton.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        deleteButton.widthAnchor.constraint(equalToConstant: 30).isActive = true
        deleteButton.heightAnchor.constraint(equalTo: deleteButton.widthAnchor).isActive = true
        deleteButton.setImage(UIImage(named: "trash"), for: .normal)
        

        nameView.leadingAnchor.constraint(equalTo: imgView.rightAnchor).isActive = true
        nameView.topAnchor.constraint(equalTo: imgView.topAnchor).isActive = true
        nameView.rightAnchor.constraint(equalTo: deleteButton.leadingAnchor).isActive = true
        nameView.textAlignment = .center
        nameView.numberOfLines = 2
        

        costView.leadingAnchor.constraint(equalTo: imgView.rightAnchor).isActive = true
        costView.topAnchor.constraint(equalTo: nameView.topAnchor).isActive = true
        costView.rightAnchor.constraint(equalTo: deleteButton.leadingAnchor).isActive = true
        costView.textAlignment = .center
        costView.numberOfLines = 2
        
        

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
*/
    override func awakeFromNib() {
        super.awakeFromNib()
        

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
