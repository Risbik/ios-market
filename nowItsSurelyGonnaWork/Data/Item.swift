//
//  Item.swift
//  nowItsSurelyGonnaWork
//
//  Created by Vladislav on 22/02/2019.
//  Copyright © 2019 Vladislav. All rights reserved.
//

import UIKit
import Foundation

class Item
{
    var name: String
    var cost: UInt32
    var desc: String
    var img: [UIImage?] = []

    
    init(_ _name: String, _ _cost: UInt32, _ _desc: String, _ _img: [UIImage?])
    {
        name = _name
        cost = _cost
        desc = _desc
        img = _img
    }
}
